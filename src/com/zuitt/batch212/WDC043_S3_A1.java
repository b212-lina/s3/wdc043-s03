package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");
        int num = scanner.nextInt();

        int x = 1;
        long factorialWhile = 1;

        if (num <= 0) {

            System.out.println("Please input a number above 0");

        } else {

            while (num != 0) {

                factorialWhile = factorialWhile * num;
                num--;

            }

            System.out.println(factorialWhile);

        }


        try {

            System.out.println("Input an integer whose factorial will be computed");
            int num1 = scanner.nextInt();

            long factorialFor = 1;

            if (num1 <= 0) {

                System.out.println("Please input a number above 0");

            } else {

                for (int i = 1; i <= num1; i++) {

                    factorialFor = factorialFor * i;

                }

                System.out.println(factorialFor);

            }

        }

        catch (Exception e) {

            System.out.println("Please input a number");

        }

    }

}
